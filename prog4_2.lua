print "Assignment #4-2, Daniel Hernandez, dhernandez92105@yahoo.com"

for i = 1, 100 do

    local fizz = 0 == i % 3
    local buzz = 0 == i % 5

    if fizz and buzz then
        print "FizzBuzz"
    elseif fizz then
        print "Fizz"
    elseif buzz then 
        print "Buzz"
    else
        print(i)
    end

end

os.exit()
