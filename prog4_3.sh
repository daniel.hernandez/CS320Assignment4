#!/bin/bash

echo "Assignment #4-3, Daniel Hernandez, dhernandez92105@yahoo.com"

file1="$1"
file2="$2"

location=`locate /lua.h | head -1`
path=${location%/lua.h}

gcc prog4_1.c -llua -lm -ldl -L $path -I $path

./a.out $file1 > output
cat $file2 > output2

if cmp -s output output2
then
    echo "Passed Test"
else
    echo "Failed Test"
fi

rm output output2
