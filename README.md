CS320 Assignment #4
===================

Objective
---------

	This assignment is intended to help us create an interactive Lua interpretor, using C code. Again, like
	previous assignments, this assignment is meant to show us and help us understand the interconnection of
	different languages and programs. We used C to create our own Lua execution environment, in order to run
	Lua code. We then used bash to automate the task of comparing files through the interpretor. 

Programs
--------
**_Names_**

	prog4_1.c

	prog4_2.lua

	prog4_3.sh

**_Description_**

*prog4_1.c*

	This is a stand-alone Lua interpretor that accepts one command line argument, a file to be run through the
	interpretor.
	
	COMPILE/RUN:
	gcc <FILE.C> -llua -lm –ldl -L <LDIR> -I <HDIR>
	./a.out <FILE.lua>

*prog4_2.lua*

	"Fizz Buzz test" program that prints numbers 1-100, but for every mutiple of 3 prints "Fizz", every mutiple of
	5 prints "Buzz", and every mutiple of 3 and 5 prints "Fizz Buzz". This lua program can be run through or Lua
	interepter (prog4_1.c).

    COMPILE/RUN:
	(see compiling and execution for prog4_1.c)
	./a.out prog4_2.lua
    
*prog4_3.sh*

	This bash script is intended to compare two files, given as two command line arguments. The first command
	line argument file is run through our lua interpretor, and the output is directed to a new output file. 
	The	second command line arguement file is concatenated, and that output is directed to another output file.
	Those files are then checked to see if the contents are the same or not. If not, "Failed Test" is displayed.
	Otherwise, "Passed Test" is displayed.

    COMPILE/RUN:
	chmod +x prog4_3.sh
    ./prog4_3.sh <FILE.lua> <FILE>
